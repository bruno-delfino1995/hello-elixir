defmodule CardsTest do
  use ExUnit.Case
  doctest Cards

  test "a new deck contains 52 cards" do
    deck_length = length Cards.create_deck
    assert deck_length == 52
  end

  test "shuffle changes the order of the deck" do
    deck = Cards.create_deck
    refute deck == Cards.shuffle deck
  end
end
