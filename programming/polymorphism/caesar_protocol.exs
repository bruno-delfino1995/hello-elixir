defprotocol Caesar do
  def encrypt(string, shift)

  def rot13(string)
end

defimpl Caesar, for: [BitString] do
  def rot13(string) do
    encrypt(string, 13)
  end

  def encrypt(string, shift) do
    string
    |> to_charlist()
    |> Caesar.encrypt(shift)
    |> to_string()
  end
end

defimpl Caesar, for: [List] do
  defmacrop is_letter(char) do
    quote do: unquote(char) in ?A..?Z or unquote(char) in ?a..?z
  end

  def rot13(charlist) do
    encrypt(charlist, 13)
  end

  def encrypt(charlist, shift) do
    charlist
    |> Enum.map(&(cipher(&1, shift)))
  end

  defp cipher(char, 0), do: char
  defp cipher(char, 26), do: char
  defp cipher(char, _number) when not is_letter(char), do: char
  defp cipher(char, number) when number > 0 and number < 26 do
    normalize_letter(char + number, uppercase?(char))
  end

  defp uppercase?(char) when is_letter(char), do: char in ?A..?Z

  defp normalize_letter(char, true) when char > ?Z, do: char - 26
  defp normalize_letter(char, false) when char > ?z, do: char - 26
  defp normalize_letter(char, _uppercase?), do: char
end

charlist = Caesar.encrypt('OMG', 5)
bynary = Caesar.encrypt("OMG", 5)
IO.puts "#{charlist} ==> #{is_list(charlist)}"
IO.puts "#{bynary} ==> #{is_binary(bynary)}"
