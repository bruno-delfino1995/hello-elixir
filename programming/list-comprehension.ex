defmodule LC do
    def prime?(2), do: true
    def prime?(number), do: Enum.all?(2..number - 1, &(rem(number, &1) != 0))

    def primes_up_to(number) do
        for n <- 2..number, prime?(n), do: n
    end

    def orders_list() do
        [
            [id: 123, ship_to: :NC, net_amount: 100.00],
            [id: 124, ship_to: :OK, net_amount: 35.50],
            [id: 125, ship_to: :TX, net_amount: 24.00],
            [id: 126, ship_to: :TX, net_amount: 44.80],
            [id: 127, ship_to: :NC, net_amount: 25.00],
            [id: 128, ship_to: :MA, net_amount: 10.00],
            [id: 129, ship_to: :CA, net_amount: 102.00],
            [id: 130, ship_to: :NC, net_amount: 50.00]
        ]
    end

    def taxes() do
        [NC: 0.075, TX: 0.08]
    end

    def apply_taxes(orders, taxes) do
        for order <- orders do
            order ++ [{
                :total_amount, order[:net_amount] + Keyword.get(taxes, order[:ship_to], 0)
            }]
        end
    end
end
