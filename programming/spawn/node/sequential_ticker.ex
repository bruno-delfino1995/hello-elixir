defmodule SequentialTicker do
  @interval 2000
  @name :sequential_ticker

  def start do
    pid = spawn(__MODULE__, :generator, [[], []])
    :global.register_name(@name, pid)
  end

  def register(client_pid) do
    IO.puts "Register #{inspect client_pid}"
    generator_pid = :global.whereis_name(@name)
    send(generator_pid, {:register, client_pid})
  end

  def generator(clients, waiting_clients) do
    receive do
      {:register, pid} ->
        IO.puts "Registering #{inspect pid}"
        tail = [pid]
        generator(clients ++ tail, waiting_clients ++ tail)
    after @interval ->
      IO.puts "Tick"
      waiting_clients = notify_clients(clients, waiting_clients)
      generator(clients, waiting_clients)
    end
  end

  defp notify_clients([], [])	, do: []
  defp notify_clients(clients, []), do: notify_clients(clients, clients)
  defp notify_clients(_clients, [next_client | waiting_clients]) do
    send(next_client, {:tick})
    waiting_clients
  end
end
