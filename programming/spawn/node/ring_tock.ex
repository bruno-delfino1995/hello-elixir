defmodule RingTock do
  @interval 2000

  def start do
    pid = spawn(__MODULE__, :wait_ack, [])

    # As the ticker pid is registered globally we need to connect
    # in the node that the ticker process was spawned. If you try
    # to register without connecting to the node you will get an
    # error saying that the ticker pid is :undefined
    RingTicker.register(pid)
  end

  def wait_ack do
    receive do
      {:registered, next_client} ->
        IO.puts "Registered, now start ticking"
        receiver(next_client)
    end
  end

  def receiver(next_client) do
    receive do
      {:change_next, client_pid} ->
        IO.puts "Change the next client"
        receiver(client_pid)
      {:tick, pid} ->
        me = inspect(self())
        sender = inspect(pid)
        node = inspect(Node.self())
        timestamp = System.system_time(:millisecond)

        IO.puts "[#{node}##{timestamp}] Tock in client #{me} received from #{sender}"
        receiver(next_client)
    after @interval ->
      send(next_client, {:tick, self()})
      receiver(next_client)
    end
  end
end
