defmodule SpawnBasic do
  def greet do
    IO.puts "Hello"
  end
end

# spawn returns a PID of the created process and call the given function, but there
# isn't any guaranty when it'll be executed
spawn(SpawnBasic, :greet, [])
