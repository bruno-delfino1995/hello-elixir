defmodule Scheduler do
  def run(num_processes, module, func, args_queue) do
    (1..num_processes)
    |> Enum.map(fn(_) -> spawn(module, func, [self()]) end)
    |> schedule_process(Enum.with_index(args_queue), [])
  end

  defp schedule_process(pids, args_queue, results) do
    receive do
      {:ready, pid} when length(args_queue) > 0 ->
        [args | tail] = args_queue
        send(pid, {:process, args})
        schedule_process(pids, tail, results)

      {:result, order, result} ->
        schedule_process(pids, args_queue, results ++ [{order, result}])

      {:ready, pid} ->
        send(pid, {:shutdown})
        if length(pids) > 1 do
          schedule_process(List.delete(pids, pid), args_queue, results)
        else
          process_results(results)
        end
    end
  end

  defp process_results(results) do
    results
    |> Enum.sort(fn({n1, _}, {n2, _}) -> n1 <= n2 end)
    |> Enum.map(&(elem(&1, 1)))
  end
end
