defmodule Chain do
  @moduledoc """
  Create a chain of processes to count from 0 to n, wherein each process
  call the next with an increment.

  ## Example
  ```bash
  $ elixir -r chain.ex -e "Chain.run(10000)"
  # Tell Erlang VM to allow up to 1 million of processes
  $ elixir --erl "+P 1000000" -r chain.ex -e "Chain.run(1_000_000)"
  ```
  """

  def counter(next_pid) do
    receive do
      n -> send(next_pid, n + 1)
    end
  end

  def create_processes(n) do
    # Create n processes configured to execute with send_to as param
    last = Enum.reduce(1..n, self(), fn(_, send_to) ->
      spawn(Chain, :counter, [send_to])
    end)

    send(last, 0)

    receive do
      final_number when is_integer(final_number)
        -> "Received #{final_number}"
    end
  end

  def run(n) do
    IO.inspect :timer.tc(Chain, :create_processes, [n])
  end
end
