defmodule MyEnum do
    def all?(list, func \\ fn x -> !!x end)
    def all?([], _func), do: true
    def all?([head | tail], func) do
        if func.(head) do
            all?(tail, func)
        else
            false
        end
    end

    def each([], _func), do: []
    def each([head | tail], func), do: [func.(head) | each(tail, func)]

    def filter([], _func), do: []
    def filter([head | tail], func) do
        if func.(head) do
            [head | filter(tail, func)]
        else
            [filter(tail, func)]
        end
    end

    def split(list, quantity), do: _split(list, [], quantity)

    defp _split([], splitted, _), do: {splitted, []}
    defp _split(list, splitted, 0), do: {splitted, list}
    defp _split([head | tail], splitted, quantity) do
        _split(tail, splitted ++ [head], quantity - 1)
    end

    def take(list, quantity), do: elem(split(list, quantity), 0)

    def flatten([]), do: []
    def flatten([head | tail]), do: flatten(head) ++ flatten(tail)
    def flatten(raw), do: [raw]
end
