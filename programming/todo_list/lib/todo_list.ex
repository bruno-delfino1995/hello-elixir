defmodule TodoList do
  @moduledoc """
  Manage a simple todo-list
  """

  defdelegate new(), to: MultiMap
  defdelegate add_entry(list, date, desc), to: MultiMap, as: :put
  defdelegate entry(list, date), to: MultiMap, as: :get
end
