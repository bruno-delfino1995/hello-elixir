defmodule Stack.Server do
  use GenServer

  def init(stash_pid) do
    current_stack = Stack.Stash.get(stash_pid)

    {:ok, {current_stack, stash_pid}}
  end

  def handle_call(:bye, _from, state), do: {:stop, :bye, state}
  def handle_call(:pop, _from, {stack, stash}) do
    [first | tail] = stack
    {:reply, first, {tail, stash}}
  end

  def handle_cast({:push, element}, _state) when element < 0, do: raise "Less than zero isn't allowed"
  def handle_cast({:push, 0}, state), do: {:stop, "Zero isn't allowed", state}
  def handle_cast({:push, element}, {stack, stash}) do
    {:noreply, {[element | stack], stash}}
  end

  def terminate(reason, {stack, stash_pid}) do
    Stack.Stash.set(stash_pid, stack)
    IO.puts "Finishing #{Stack.Server} due to '#{inspect(reason)}' - stack = #{inspect(stack)}"
  end
end
