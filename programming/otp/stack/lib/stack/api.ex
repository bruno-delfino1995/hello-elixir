defmodule Stack.API do
  def start_link(stash_pid) do
    GenServer.start_link(Stack.Server, stash_pid, name: __MODULE__)
  end

  def push(elem) do
    GenServer.cast(__MODULE__, {:push, elem})
  end

  def pop() do
    GenServer.call(__MODULE__, :pop)
  end
end
