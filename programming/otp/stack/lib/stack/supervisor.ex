defmodule Stack.Supervisor do
  use Supervisor

  def start_link(initial_stack) do
    supervisor = {:ok, sup_pid} = Supervisor.start_link(__MODULE__, nil)

    start_workers(sup_pid, initial_stack)

    supervisor
  end

  defp start_workers(sup_pid, initial_stack) do
    {:ok, stash_pid} = Supervisor.start_child(sup_pid, worker(Stack.Stash, [initial_stack]))
    {:ok, _api_pid} = Supervisor.start_child(sup_pid, worker(Stack.API, [stash_pid]))
  end

  def init(_) do
    supervise([], strategy: :one_for_one)
  end
end
