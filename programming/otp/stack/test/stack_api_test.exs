defmodule Stack.APITest do
  use ExUnit.Case

  alias Stack.API

  test "after started should be acessible by module name" do
    {:ok, pid} = API.start_link()

    assert elem(:sys.get_status(API), 1) == pid
  end

  test "has a interface to push items" do
    API.start_link()

    assert API.push(2) == :ok
  end

  test "has a interface to pop items" do
    API.start_link()

    API.push(2)

    assert API.pop() == 2
  end
end
