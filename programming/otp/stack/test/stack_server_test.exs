defmodule Stack.ServerTest do
	use ExUnit.Case

	test "should accept calls to remove the last element" do
		{:ok, pid} = GenServer.start_link(Stack.Server, [5, "cat", 9])

		assert GenServer.call(pid, :pop) == 5
	end

	test "should accept cast calls to push a new element" do
		{:ok, pid} = GenServer.start_link(Stack.Server, [5])

    GenServer.cast(pid, {:push, 6})

    assert GenServer.call(pid, :pop) == 6
    assert GenServer.call(pid, :pop) == 5
	end
end
