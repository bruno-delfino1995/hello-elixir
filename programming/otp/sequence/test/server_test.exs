defmodule Sequence.ServerTest do
	use ExUnit.Case

	test "should accept calls to increment the current number" do
		{:ok, pid} = GenServer.start_link(Sequence.Server, 0)

		assert GenServer.call(pid, :next_number) == 1
	end
end