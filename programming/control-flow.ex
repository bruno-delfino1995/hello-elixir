defmodule CF do
    def fizz_buzz(number) when rem(number, 3) == 0 and rem(number, 5) == 0 do
        "FizzBuzz"
    end
    def fizz_buzz(number) when rem(number, 3) == 0, do: "Fizz"
    def fizz_buzz(number) when rem(number, 5) == 0, do: "Buzz"
    def fizz_buzz(number), do: number

    # cond is like a multiple if, it seems like switch from other languages but
    # it doesn't match against a predefined value, it stops on the first truthy condition
    def cond_fizz_buzz(number) do
        cond do
            rem(number, 3) and rem(number, 5) -> "FizzBuzz"
            rem(number, 3) -> "Fizz"
            rem(number, 5) -> "Buzz"
            true -> number
        end
    end

    # case is more like switch because it uses a predefined value matched against
    # each condition using pattern matching, it stops on the first match
    def case_fizz_buzz(number) do
        case {rem(number, 3), rem(number, 5)} do
            {0, 0} -> "FizzBuzz"
            {0, _} -> "Fizz"
            {_, 0} -> "Buzz"
            _ -> number
        end
    end

    def ok!({:error, reason}), do: raise reason
    def ok!({:ok, result}), do: result
end
