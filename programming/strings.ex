defmodule Strings do
    # We can iterate over char lists using comprehensions or recursion
    def only_ascii?([head | []]) when head in ?\s..?~, do: true
    def only_ascii?([head | tail]) when head in ?\s..?~, do: only_ascii?(tail)
    def only_ascii?(_), do: false

    # Since single quoted strings are, in depth, character lists, we can use modules
    # that manipulate lists to work with them
    def anagram?(word, match), do: Enum.sort(word) == Enum.sort(match)

    # iex> [ 'cat' | 'dog' ]
    # ['cat',100,111,103]
    # IEx prints that because the head of the list is a list itself, but the outer list
    # isn't only composed of printable characters. The first element of the list is
    # another list, so if we flatten the list we could get the words concatenated

    def calculate(expr) do
        {n1, rest} = parse_number(expr)
        rest = remove_leading_spaces(rest)
        {func, rest} = parse_operator(rest)
        rest = remove_leading_spaces(rest)
        {n2, _} = parse_number(rest)

        func.(n1, n2)
    end

    defp parse_number(expr), do: do_parse_number(expr, 0)
    defp do_parse_number([], value), do: {value, []}
    defp do_parse_number([head | tail], value) when head in '012345679' do
        do_parse_number(tail, value * 10 + (head - ?0))
    end
    defp do_parse_number(rest, value), do: {value, rest}

    def remove_leading_spaces([?\s | tail]), do: remove_leading_spaces(tail)
    def remove_leading_spaces(rest), do: rest

    defp parse_operator([?+ | rest]), do: {&(&1 + &2), rest}
    defp parse_operator([?- | rest]), do: {&(&1 - &2), rest}
    defp parse_operator([?* | rest]), do: {&(&1 * &2), rest}
    defp parse_operator([?/ | rest]), do: {&(&1 / &2), rest}

    def parse_int([?- | value]), do: parse_int(value) * -1
    def parse_int([?+ | value]), do: parse_int(value)
    def parse_int(string), do: do_parse_int(string, 0)
    defp do_parse_int([], value), do: value
    defp do_parse_int([head | tail], value) when head in '0123456789' do
        do_parse_int(tail, value * 10 + (head - ?0))
    end

    def print_centered(strings) do
        line_length = String.length(longest(strings))

        strings
        |> Enum.map(&(center(&1, line_length)))
        |> Enum.each(&IO.puts/1)
    end

    def center(string, length, padding \\ " ") do
        spaces = (length - String.length(string))
        left = div(spaces, 2)
        right = spaces - left

        String.duplicate(padding, left) <> string <> String.duplicate(padding, right)
    end

    def longest([head | tail]), do: do_longest(tail, head)
    defp do_longest([], longest), do: longest
    defp do_longest([head | tail], longest) do
        if String.length(head) > String.length(longest) do
            do_longest(tail, head)
        else
            do_longest(tail, longest)
        end
    end

    def capitalize_sentences(sentence) do
        sentence
        |> String.split(~r/\./)
        |> Enum.map(&(
            &1
            |> String.trim()
            |> String.capitalize()
        ))
        |> Enum.join(". ")
    end

    def apply_taxes_from_file(filename) do
        File.open!(filename, [:read])
        |> IO.stream(:line)
        |> Stream.map(&to_order_list/1)
        |> LC.apply_taxes(LC.taxes())
    end

    defp to_order_list(data) do
        [id, ship_to, net_amount] = data
        |> String.split(~r/,/)
        |> Enum.map(&String.trim/1)

        [
            id: converter(id),
            ship_to: converter(ship_to),
            net_amount: converter(net_amount)
        ]
    end

    def converter(value) do
        cond do
            Regex.match?(~r/^\d+$/, value) -> String.to_integer(value)
            Regex.match?(~r/^\d+\.\d+$/, value) -> String.to_float(value)
            << ?: :: utf8, name :: binary >> = value -> String.to_atom(name)
            true -> value
        end
    end
end
