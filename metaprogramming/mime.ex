defmodule Mime do
  # By declaring this attribute the code will recompile when the resource
  # changes, instead of only on code changes
  @external_resource mimes_path = Path.join([__DIR__, "mimes.txt"])

  ext_mapping =
    for line <- File.stream!(mimes_path, [], :line) do
      line
      |> String.split("\t")
      |> Enum.map(&String.strip/1)
      |> List.to_tuple()
    end

  ext_mapping =
    ext_mapping
    |> Enum.reduce(%{}, fn {ext, type}, acc ->
      Map.update(acc, type, [ext], fn exts -> [ext | exts] end)
    end)
    |> Map.to_list()

  for {type, exts} <- ext_mapping do
    def exts_from_type(unquote(type)), do: unquote(exts)
    def type_from_exts(ext) when ext in unquote(exts), do: unquote(type)
  end

  def exts_from_type(_type), do: []
  def type_from_exts(_ext), do: nil
  def valid_type?(type), do: exts_from_type(type) |> Enum.any?()
end
