ExUnit.start
Code.require_file("translator.ex", __DIR__)

defmodule TranslatorTest do
  use ExUnit.Case

  # Test your code generated, not your code generation
  defmodule I18n do
    use Translator

    locale "en", [
      name: "Peter",
      flash: [
        hello: "Hello %{first} %{last}!",
        bye: [
          informal: "Bye",
          formal: "Bye"
        ]
      ]
    ]

    locale "br", [
      name: "Pedro",
      flash: [
        hello: "Olá %{first} %{last}!",
        bye: [
          informal: "Falou",
          format: "Tchau"
        ]
      ]
    ]
  end

  describe "t/3" do
    test "it walks recursively through the translation three" do
      assert I18n.t("en", "flash.bye.formal") == "Bye"
      assert I18n.t("br", "flash.bye.informal") == "Falou"
    end

    test "it handles translations at root level" do
      assert I18n.t("en", "name") == "Peter"
    end

    test "it has a fallback for undefined translations" do
      assert I18n.t("fr", "name") == {:error, :no_translation}
    end

    test "it interpolates bindings" do
      assert I18n.t("en", "flash.hello", first: "Bruno", last: "Delfino")
    end

    test "it convert bindings to string" do
      assert I18n.t("br", "flash.hello", first: "João", last: 3)
    end

    test "it raises KeyError when binding not provided" do
      assert_raise KeyError, fn -> I18n.t("en", "flash.hello", first: "Bruno") end
    end
  end

  describe "compile/1" do
    # Be careful while testing code generation, as the AST is hard to match
    # and subtle to changes, testing it can lead to error-prone tests
    test "it generates chatch-all for t/3 functions" do
      assert Translator.compile([]) |> Macro.to_string == String.strip ~S"""
      (
        def(t(locale, path, bindings \\ []))
        []
        def(t(_locale, _path, _bindings)) do
          {:error, :no_translation}
        end
      )
      """
    end

    test "it generates t/3 functions for each locale" do
      locales = [{"en", [foo: "bar", bar: "%{baz}"]}]
      ast = quote context: Translator do
        def t(locale, path, bindings \\ [])
        [
          [
            (def t("en", "foo", bindings), do: "" <> "bar"),
            (def t("en", "bar", bindings), do: ("" <> to_string(Keyword.fetch!(bindings, :baz))) <> "")
          ]
        ]
        def t(_locale, _path, _bindings), do: {:error, :no_translation}
      end

      assert Translator.compile(locales) == ast
    end
  end
end
