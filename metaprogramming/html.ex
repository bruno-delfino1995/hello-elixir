

defmodule Html do
  @external_resource tags_path = Path.join([__DIR__, "html-tags.txt"])
  @tags (for line <- File.stream!(tags_path, [], :line) do
    line |> String.strip |> String.to_atom
  end)

  defmacro markup(do: block) do
    quote do
      import Kernel, except: [div: 2]
      {:ok, var!(buff, unquote(__MODULE__))} = start_buffer([])
      unquote(Macro.postwalk(block, &postwalk/1))
      result = render(var!(buff, unquote(__MODULE__)))
      :ok = stop_buffer(var!(buff, unquote(__MODULE__)))
      result
    end
  end

  def postwalk({:text, _meta, [content]}) do
    quote do: put_buffer(var!(buff, unquote(__MODULE__)), to_string(unquote(content)))
  end
  def postwalk({tag_name, _meta, [[do: inner]]}) when tag_name in @tags do
    quote do: tag(unquote(tag_name), do: unquote(inner))
  end
  def postwalk({tag_name, _meta, [attrs, [do: inner]]}) when tag_name in @tags do
    quote do: tag(unquote(tag_name), unquote(attrs), do: unquote(inner))
  end
  def postwalk({tag_name, _meta, attrs}) when tag_name in @tags do
    quote do: tag(unquote(tag_name), unquote(attrs))
  end
  def postwalk(ast), do: ast

  defmacro tag(name, attrs \\ []) do
    {inner, attrs} = Keyword.pop(attrs, :do)
    quote do: tag(unquote(name), unquote(attrs), do: unquote(inner))
  end
  defmacro tag(name, attrs, do: content) do
    quote do
      put_buffer(var!(buff, unquote(__MODULE__)), open_tag(unquote_splicing([name, attrs])))
      unquote(Macro.postwalk(content, &postwalk/1))
      put_buffer(var!(buff, unquote(__MODULE__)), "</#{unquote(name)}>")
    end
  end

  def open_tag(name, []), do: "<#{name}>"
  def open_tag(name, attrs) do
    attrs_html = for {key, val} <- attrs, into: "", do: " #{key}=\"#{val}\""

    "<#{name}#{attrs_html}>"
  end

  def start_buffer(state), do: Agent.start_link(fn -> state end)
  def stop_buffer(buff), do: Agent.stop(buff)
  def put_buffer(buff, content), do: Agent.update(buff, &[content | &1])
  def render(buff), do: Agent.get(buff, &(&1)) |> Enum.reverse |> Enum.join
end
