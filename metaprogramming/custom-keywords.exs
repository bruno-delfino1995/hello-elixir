defmodule CustomKeywords do def cif_func(condition, clauses) do
    do_clause = Keyword.get(clauses, :do, nil)
    else_clause = Keyword.get(clauses, :else, nil)

    case condition do
      val when val in [false, nil] -> else_clause
      _otherwise -> do_clause
    end
  end

  defmacro if(condition, clauses) do
    do_clause = Keyword.get(clauses, :do, nil)
    else_clause = Keyword.get(clauses, :else, nil)

    quote do
      case unquote(condition) do
        val when val in [false, nil] -> unquote(else_clause)
        _otherwise -> unquote(do_clause)
      end
    end
  end

  defmacro unless(condition, clauses) do
    do_clause = Keyword.get(clauses, :do, nil)
    else_clause = Keyword.get(clauses, :else, nil)

    quote do
      if !unquote(condition) do
        unquote(do_clause)
      else
        unquote(else_clause)
      end
    end
  end

  defmacro while(condition, do: body) do
    quote do
      try do
        for _ <- Stream.cycle([:ok]) do
          if unquote(condition) do
            unquote(body)
          else
            unquote(__MODULE__).break
          end
        end
      catch
        :break -> :ok
      end
    end
  end

  def break, do: throw :break
end

# As the code is evaluated before passing it to the func we see in terminal
# both outputs, what is being passed to our function is the result of the
# evaluation of the clauses, so the result is `cif_func 1 == 2, do: :ok, else: :ok`
CustomKeywords.cif_func 1 == 2 do
  IO.puts "1 == 2"
else
  IO.puts "1 != 2"
end

defmodule Test do
  require CustomKeywords

  IO.puts "Here i'm using the macro, so only the else code will be executed"

  CustomKeywords.if 1 == 2 do
    IO.puts "1 == 2"
  else
    IO.puts "1 != 2"
  end

  CustomKeywords.unless 1 == 1 do
    IO.puts "It's true, do not execute me"
  else
    IO.puts "Execute me :P"
  end

  CustomKeywords.unless 1 == 2 do
    IO.puts "Now I'm supposed to be executed"
  else
    IO.puts "T_T"
  end

  pid = spawn(fn -> :timer.sleep(4000) end)
  CustomKeywords.while Process.alive?(pid) do
    IO.puts "Process alive :D"
    :timer.sleep(1000)
  end

  end_time = System.os_time(:seconds) + 10
  CustomKeywords.while end_time > System.os_time(:seconds) do
    IO.puts "Print before time runs outs -> #{System.os_time(:seconds)}"
  end
end
