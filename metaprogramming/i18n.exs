defmodule I18n do
  use Translator

  locale "en",
    flash: [
      hello: "Hello %{first} %{last}!",
      bye: "Bye, %{name}!"
    ],
    users: [
      title: "Users"
    ]

  locale "fr",
    flash: [
      hello: "Salut %{first} %{last}!",
      bye: "Au revoir, %{name}!"
    ],
    users: [
      title: "Utilisateurs"
    ]
end

# > "Hello Bruno Delfino!"
IO.inspect I18n.t("en", "flash.hello", first: "Bruno", last: "Delfino")

# > "Salut Bruno Delfino!"
IO.inspect I18n.t("fr", "flash.hello", first: "Bruno", last: "Delfino")

# > "Users"
IO.inspect I18n.t("en", "users.title")
