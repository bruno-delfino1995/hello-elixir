defmodule ExTest.Assertion do
  defmodule AssertionError do
    defexception message: "Expected x, got y"
  end

  defmacro assert({operator, _, [lhs, rhs]}) do
    quote bind_quoted: [
      operator: operator,
      lhs: lhs,
      rhs: rhs
    ] do
      ExTest.Assertion.ensure(lhs, operator, rhs)
    end
  end

  def ensure(lhs, operator, rhs) do
    if apply(Kernel, operator, [lhs, rhs]) do
      :ok
    else
      explode(operator, lhs, rhs)
    end
  end

  defp explode(operator, lhs, rhs) do
    raise __MODULE__.AssertionError,
      message: "Expected #{lhs} to be #{operator} to #{rhs}"
  end
end
