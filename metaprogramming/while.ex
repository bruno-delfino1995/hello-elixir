defmodule While do
  defmacro while(condition, do: body) do
    quote do
      try do
        for _ <- Stream.cycle([:ok]) do
          if unquote(condition) do
            unquote(body)
          else
            unquote(__MODULE__).break
          end
        end
      catch
        :break -> :ok
      end
    end
  end

  def break, do: throw :break
end
