defmodule Explainer do
  defmacro is_raw(left, right) do
    quote do
      is_number(unquote(left)) and is_number(unquote(right))
    end
  end

  defmacro explain(expression), do: IO.puts humanize(expression)

  defp humanize({operator, _metadata, [left, right]}) when is_raw(left, right) do
    operation_name = operation_to_action(operator)

    "#{left} #{operation_name} #{right}"
  end
  defp humanize({operator, metadata, [left, right]}) when is_number(left) do
    humanize({operator, metadata, [right, left]})
  end
  defp humanize({operator, _metadata, [left, right]}) when is_number(right) do
    operation_name = operation_to_action(operator)
    explanation = humanize(left)

    "#{explanation} then #{operation_name} #{right}"
  end
  defp humanize({operator, _metadata, [left, right]}) do
    operation_name = operation_to_action(operator)
    left_explanation = humanize(left)
    right_explanation = humanize(right)

    "first #{left_explanation}, then #{right_explanation}, then #{operation_name} both"
  end

  @operation_to_action %{
    +: "add",
    -: "subtract",
    *: "multiply",
    /: "divide"
  }
  def operation_to_action(op) do
    Map.get(@operation_to_action, op)
  end
end

defmodule Test do
  require Explainer

  Explainer.explain(1 + 2)
  Explainer.explain(1 + 2 * 3)
  Explainer.explain(2 * 3 + 1)
  Explainer.explain(1 * 2 + 2 * 3)
  Explainer.explain(5 * (2 + 3))
  Explainer.explain(5 * (2 * 2 + 3 * 3))
end
