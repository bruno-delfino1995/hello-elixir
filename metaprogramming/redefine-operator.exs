defmodule Operators do
  defmacro left + right do
    quote do
      to_string(unquote(left)) <> to_string(unquote(right))
    end
  end
end

defmodule Test do
  IO.puts "Normal plus operation #{123 + 456} => Kernel.+/2"

  import Kernel, except: [+: 2]
  import Operators

  IO.puts "Overriden plus operation #{123 + 456} => Operators.+/2"
end
